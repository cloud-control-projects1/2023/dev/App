package com.company.emptyamplrpoject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class EmptyAmplRpojectApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmptyAmplRpojectApplication.class, args);
    }
}
