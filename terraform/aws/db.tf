module "empty-ampl-rpoject_db" {
  source = "./db"

  name                                  = var.empty-ampl-rpoject_db_name
  engine                                = var.empty-ampl-rpoject_db_engine
  engine_version                        = var.empty-ampl-rpoject_db_engine_version
  instance_class                        = var.empty-ampl-rpoject_db_instance_class
  storage                               = var.empty-ampl-rpoject_db_storage
  user                                  = var.empty-ampl-rpoject_db_user
  password                              = var.empty-ampl-rpoject_db_password
  random_password                       = var.empty-ampl-rpoject_db_random_password
  vpc_id                                = module.vpc.id
  subnet_group_name                     = module.vpc.db_subnet_group_name
  multi_az                              = var.empty-ampl-rpoject_db_multi_az
  source_security_group_id              = module.beanstalk.aws_security_group.id
}
