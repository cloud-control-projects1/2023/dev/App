module "ssl" {
  source = "./self_signed"

  aws_elastic_beanstalk_environment = module.beanstalk.aws_elastic_beanstalk_environment
  name                              = var.name
  region                            = var.region
}
